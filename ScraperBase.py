__author__ = 'vandermonde'

import bs4 as bs
from Crawler import Crawler
from utils import utils

class imageTagStruct:
    """just a data structure, containing list of tags and list of images corresponding to"""

    def __init__(self):
        pass


class ScraperBase:
    """base scraping class"""

    crawler = Crawler()

    def __init__(self, url):
        self.__url =  url

    def extractCategories(self):
        pass

    def checkSiteUpdate(self):
        pass

    def scrapeWholeSite(self):
        pass

class ScrapeHani(ScraperBase):
    """scraping class for http://www.modelhani1.blogfa.com/ website"""

    def __init__(self):
        self.url = "http://www.modelhani1.blogfa.com/"

    def checkSiteUpdate(self):
        pass

    def extractCategories(self,url):

        content = self.crawler.getUrlContent(url)

        soup = bs.BeautifulSoup(content)

        #print soup.prettify()
        categories = soup.find_all("div",{"class":"catimg"})
        urls = []
        for link in categories[0].find_all("a"):
            urls.append(url[0:-1] + link.get("href") )

        return urls


    def getContinueReadingLinks(self , url):
        content = self.crawler.getUrlContent(url)
        soup = bs.BeautifulSoup(content)
        mores = soup.find_all("h3", {"class":"more"})
        urls = []
        for more in mores:
            urls.append(self.url[:-1] + more.a.get("href"))


        return urls

    def scrapePage(self,url):
        content = self.crawler.getUrlContent(url)

        soup = bs.BeautifulSoup(content)
        centerPics = soup.find_all("div",{"class":"Clear center-mian-bg"})[1]
        imageTag = centerPics.find("h5").contents[2].string.strip()
        images = [img.get("src") for img in centerPics.find_all("img")]

        datas = imageTagStruct()
        datas.images  = images
        datas.tags = [imageTag]

        return datas





    def scrapeWholeSite(self):
        urls = self.extractCategories(self.url)

        iii = 0
        for url in urls:
            continueReadings = self.getContinueReadingLinks(url)
            for continueReading in continueReadings:
                rawData = self.scrapePage(continueReading)
                print("tags: "+ str(rawData.tags ))
                print("images: " + str(rawData.images))
                utils.saveImageArray(rawData.images)
                iii +=1
                if(iii == 3):
                    return



scrapeHani = ScrapeHani()
scrapeHani.scrapeWholeSite()