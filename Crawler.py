__author__ = 'vandermonde'

import requests

class Crawler:
    """ the crawler class
    """
    def __init__(self):
        pass

    def getUrlContent(self, url):
        return requests.get(url).text

    @staticmethod
    def downloadImage( url):
        result = requests.get(url , stream=True)
        if(result.status_code == 200):
            return result

        return -1