__author__ = 'vandermonde'

from Crawler import  Crawler
import os



class utils:
    """utilities needed for scraper"""


    staticsDir = "statics/"

    @staticmethod
    def saveImageArray(images):
       for imageUrlInstance in images:
           utils.saveImage(imageUrlInstance)

    @staticmethod
    def saveImage(image):
        imageContent = Crawler.downloadImage(image)

        imageDir , imageFileName = utils.separateDirAndFileFromUrl(image)
        utils.ensureDir(utils.staticsDir+imageDir)
        with open(utils.staticsDir+imageDir+imageFileName, "wb") as imageFile :
            for chunk in imageContent.iter_content(1024):
                imageFile.write(chunk)


    @staticmethod
    def ensureDir(dir):
        if not os.path.exists(dir):
            os.makedirs(dir)

    @staticmethod
    def separateDirAndFileFromUrl(url):
        imageDir = url[7:]
        firstSlashLoc = imageDir.rfind("/")
        imageFilename = imageDir[firstSlashLoc+1:]
        imageDir = imageDir[:firstSlashLoc+1]
        return (imageDir,imageFilename)