__author__ = 'vandermonde'

from peewee import *

database = MySQLDatabase.connect(host="127.0.0.1", user="scraper", passwd="scraperPass", db="scraper")

class BaseModel(Model):
    """ base data model inherited by all other models
    """
    class Meta:
        database = database


class Image(BaseModel):
    """ cloth model
    """

    id = PrimaryKeyField(primary_key= True)
    url = TextField()
    dir = CharField()
    name= CharField()
    publishDate = DateTimeField(formats='%Y-%m-%d %H:%M:%S.%f',null=True)
    downloadDate = DateTimeField(formats='%Y-%m-%d %H:%M:%S.%f')


    class Meta:
        order_by = ('publishDate',)



class Tag(BaseModel):
    """ tags model
    """

    id = PrimaryKeyField(primary_key= True)
    key = CharField()
    findDate = DateTimeField(formats='%Y-%m-%d %H:%M:%S.%f')

    class Meta:
        order_by = ('findDate',)


class ImageTag(BaseModel):
    """ image tag many to many relationship is depicted here
    """

    image = ForeignKeyField(Image, 'id')
    tag = ForeignKeyField(Tag, 'id')



def createTables():
    Image.create_table(True)
    Tag.create_table(True)
    ImageTag.create_table(True)



class Delegaro:
    """ delegator class is responsible for querying database
    """

    def saveImage(self, url, dir, name, downloadDate, publishDate=None):
        image = Image.create(url=url, dir=dir, name=name, downloadDate=downloadDate, publishDate=publishDate)


    def saveTag(self, key, findDate):
        tag = Tag.create(key=key, findDate = findDate)

    def saveImageTag(self, ):
        pass

